import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class SortingUtilities {
	public static void main(String[] args) {
		List<HashMap<String, Integer>> beforeSortingHashMap = new ArrayList<>();

		HashMap<String, Integer> hm1 = new HashMap<>();
		HashMap<String, Integer> hm2 = new HashMap<>();
		HashMap<String, Integer> hm3 = new HashMap<>();
		HashMap<String, Integer> hm4 = new HashMap<>();
		// HashMap<String, Integer> hm5 = new HashMap<>();
		hm1.put("p2", 5);
		hm1.put("p1", 21);

		hm2.put("p1", 21);
		hm2.put("p2", 1);

		hm3.put("p1", 6);
		hm3.put("p2", 9);

		hm4.put("p1", 6);
		hm4.put("p2", 1);

		// hm5.put("p1", 22);

		beforeSortingHashMap.add(hm1);
		beforeSortingHashMap.add(hm2);
		beforeSortingHashMap.add(hm3);
		beforeSortingHashMap.add(hm4);
		// beforeSortingHashMap.add(hm5);

		System.out.println("Before::: sorting :: value::" + beforeSortingHashMap);
		System.out.println("Before::: sorting :: Size::" + beforeSortingHashMap.size());
		List<HashMap<String, Integer>> afterSoritngHashMap = sortingListOfHashMapByValue(beforeSortingHashMap, true);
		System.out.println("After ::: sorting :: value::" + afterSoritngHashMap);
		System.out.println("After ::: sorting :: Size::" + afterSoritngHashMap.size());

	}

	public static List<HashMap<String, Integer>> sortingListOfHashMapByValue(
			List<HashMap<String, Integer>> beforeSortingHashMapList, boolean isAscendingOrder) {
		int order = isAscendingOrder ? -1 : 1;
		Comparator<HashMap<String, Integer>> valueComparator = new Comparator<HashMap<String, Integer>>() {
			public int compare(HashMap<String, Integer> obj1, HashMap<String, Integer> obj2) {

				int value = 1 * order;
				for (Integer obj1v : obj2.values()) {
					for (Integer obj2v : obj1.values()) {
						value = obj1v.compareTo(obj2v);
						//System.out.println("obj1v:"+obj1v+" obj2v:"+obj2v+" value:"+value);
						if (value == 0) {
							continue;
						} else {
							return value * order;
						}
					}
				}
				return value;

			}
		};

		Collections.sort(beforeSortingHashMapList, valueComparator);

		// System.out.println(beforeSortingHashMapList);
		return beforeSortingHashMapList;
	}

}
